 <div class="container-fluid mx-0 px-0">
            <footer id="contact" class="bg-dark rounded-top pb-5 mx-0">
                <div class="row mx-0 py-5 text-center">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 icon-small-f">
                        <a href="https://www.facebook.com/tomasz.zajdel.35" target="blank"><i class="light icon-facebook-circled-1"></i></a>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 icon-small">
                        <a href="https://www.google.com/intl/pl/gmail/about/" target="blank"><i class="light icon-mail-circled"></i></a>
                    </div>
                </div>
                <div class="row mx-auto py-5 px-4">
                    <form class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 mx-auto" method="post" action="content/form.php">
                        <div class='form-group'>
                            <label for='email' class="text-light">Skontaktuj się z nami:</label>
                            <input type="email" class="form-control form-control-md" name='mail' id="email" placeholder="Podaj swój e-mail">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control form-control-md" name='topic' id="topic" placeholder="Temat">
                        </div>
                        <div class="form-group">
                            <textarea type="textarea" class="form-control form-control-md" name='text' id="tArea" placeholder="wiadomość do nas" style="max-height: 150px !important; "></textarea>
                            <button type="submit" class="btn btn-primary text-light mt-2 px-3 py-2">Wyślij</button>
                        </div>
                    </form>
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 mx-auto">
                        <h4 class="light">Kontakt</h4>
                        <div class="text-light px-5">
                            Żagań, 68-100<br/> Województwo: Lubuskie<br/> ul. Klonowa 19/5.<br/>
                            <hr/>
                            e-mail: tomasz.zajdel1@gmail.com<br/>
                            <a href="tel:+48661651079" class="phone light" disabled><i class="fas fa-phone-volume text-success"></i>  Nr. tel: 661 651 079</a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 mx-auto">
                        <h4 class="light">Dane Firmy</h4>
                        <div class="text-light px-5">
                            Tomasz Zajdel<br/> Przedsiębiorstwo <br/>Usługowo-Handlowe "Elektros"<br/>
                            <hr/>
                            NIP: 924-184-10-88<br/> REGON: 081192166
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 mx-auto">
                        <h4 class="light">Pozostałe</h4>
                        <div class="text-light px-5">
                            <h6>Wykonanie strony internetowej:<br/>
                                Webmaster Albert Jurasik<br/>
                                e-mail: nanquery@gmail.com</h6>

                        </div>
                        <hr/>
                    </div>
                </div>
            </footer>
        </div>
        <!-- Scripts-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous" async></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"  crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous" async></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous" async></script>
        <script src="js/display.js" async></script>
        <script src="js/lightbox/js/lightbox-plus-jquery.min.js" async></script>
