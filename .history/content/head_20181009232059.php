﻿
        
  <meta charset="UTF-8">
        <meta name="description" content="
              Przedsiębiorstwo Usługowo-handlowe Elektros od elektroniki w Żaganiu. Profesjonalna firma naprawia awarie, montuje i wymienia instalacje elektryczne. Pełen zakres usług">
        <title>Elektryk Żagań Przedsiębiorstwo Usługowo-Handlowe ELEKTROS</title>
        <meta name="keywords" content="Tomasz, Zajdel, Żagań, Zagan, okolice, przydatne Sagan, elektryk, usługowo, Usługowo, usługa, usługi elektros, elektryka, elektros-24, elektronika, elektroniki, 24, doba, całodobowo, calodobowo, calo-dobowo, montaż, wykonanie, profesjonalizm, profesjonalna, pełna, pełen, zakres, oferta, oferty, praca, firma, żagań, okolice, budowa, dom, ogród, ogrod, elewacja, budynki, usługi, przedsiębiorstwo, usługowo-handlowe, handlowe, usługowe, malowanie, elewacja, lubuskie, montuje, montaż, montaz, montażu, przygotowanie, obszerny, prace, pro, u, nas, firma, przedsiebiorstwo, zakład, zaklad, elektroinstalarstwo, elektroinstalacje, elektroinstalacja, instalacje, instalacja, instalacji, oświetlenie, oswietlenie, odgromowe, odgromowych, uswanie, awarie, awaria, pomiary, odbiorcze, okresowe, natężenia, natezenia, bhp, BHP, bezpieczństwo, i, higiena, pracy, praca, bezpieczne, wymienia">
        <!--Work for me SEO! -->
        <meta name="author" content="Tomasz Zajdel">
        <!-- -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="robots" content="index" />
		  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
        <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link rel="shortcut icon" type="image/x-icon" href="img/icon.png">
        <link rel="stylesheet" href="css/main.css" type="text/css">
        <link rel="stylesheet" href="css/fontello.css" type="text/css">
        <link href="js/lightbox/css/lightbox.css" rel="stylesheet">
        <link href="js/lightbox/css/lightbox.min.css" rel="stylesheet"> 
        <link rel="”alternate”" hreflang="”pl”" href="”https://elektros-24.nazwa.pl/”">  