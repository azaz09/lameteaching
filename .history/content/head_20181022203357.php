﻿
        
  <meta charset="UTF-8">
        <meta name="description" content="">
               <title>JS - ćwiczenia</title>
        <meta name="keywords" content="">
        <!--Work for me SEO! -->
        <meta name="author" content="Tomasz Zajdel">
        <!-- -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="robots" content="index" />
		  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
        <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link rel="shortcut icon" type="image/x-icon" href="img/icon.png">
        <link rel="stylesheet" href="css/main.css" type="text/css">
        <link rel="stylesheet" href="css/fontello.css" type="text/css">
        <link href="js/lightbox/css/lightbox.css" rel="stylesheet">
        <link href="js/lightbox/css/lightbox.min.css" rel="stylesheet"> 
        <link rel="”alternate”" hreflang="”pl”" href="”https://elektros-24.nazwa.pl/”">  