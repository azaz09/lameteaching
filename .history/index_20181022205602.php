<?php session_start(); ?>

<!--Webdev Albert Jurasik-->
<!DOCTYPE Html>

<html lang="pl">
    <head>
        <?php
        require('content/head.php');
        ?>
    </head>
    <body>
        <div class="container">
            <header class="row bg-info border border-light rounded my-5">
                <div class="col-lg-12 col-md-12 col-sm-12 mx-auto my-auto py-5 text-center">Js - ćwiczenia</div>
            </header>
          <div>
        <div class="container-fluid">
            <div class="row text-center">
                <div class="col-lg-4 col-md-4 col-sm-4 mx-auto">
                    <div class="program col-lg-12 col-md-12 col-sm-12 border border-light rounded py-3">
                        <h4>Pierwszy program</h4>
                        <button type="button" class="btn btn-info btn-lg">Klik!</button>
                    </div>
                    
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 mx-auto">
                     <div class="program col-lg-12 col-md-12 col-sm-12 border border-light rounded py-3">
                        <h4>Drugi program</h4>
                        <button type="button" class="btn btn-success btn-lg">Klik!</button>
                    </div>
                    
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 mx-auto">
                    <div class="program col-lg-12 col-md-12 col-sm-12 border border-light rounded py-3">
                        <h4>Trzeci program</h4>
                        <button type="button" class="btn btn-danger btn-lg">Klik?</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end-->
         <?php
            require('content/end.php')
            ?>
    </body>
</html>