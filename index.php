<?php
session_start();
$_SESSION['active'] = 1;
?>
<!DOCTYPE html>
<!--Webdev Albert Jurasik-->
<html lang="pl">
    <head>
        <?php require_once('common/head.php'); ?>
    </head>
    <body>

        <?php require_once('nav-temp.php'); ?>
        <div id="js-page">
            <?=
            isset($_SESSION['send']) ? " <div class='alert alert-success alert-dismissible fade show' role='alert'>
                    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                    Wiadomość wysłana.
                </div>" : "";
            unset($_SESSION['send']);
            ?>
            <?php require_once("views/main.php"); ?>

        </div>

        <footer class="text-light">
            <?php
            include('views/footer.php');
            require_once 'common/end.php';
            ?>
        </footer>
        <div class="row" style="background-color: #393d45;">

            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 ">
                <div class="alert rounded-0 alert-dismissible fade show mb-0 text-right text-light center-block"  role="alert">
                    Kontynuując korzystanie ze strony, wyrażasz zgodę na używanie ciasteczek w celu polepszenia działania witryny.
                    <button type="button" class="btn lvly-blue btn-sm text-light mx-auto" data-dismiss="alert" aria-label="Close">
                        Zgadzam się
                    </button>
                </div>
            </div>
        </div>
    </body>
</html>
