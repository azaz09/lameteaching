<nav class="pepper-red py-2 top-bar nav-expand-lg nav-temp nav-top-fixed mt-0">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                <div class="btn btn-top float-right mx-2">
                    <a role="button" href="views/contact.php">
                        Kontakt</a> 
                </div>
                <div class="btn btn-top float-right mx-2">
                    <a role="button" href="views/lore.php">
                        Wiedza</a>
                </div>
                <div class="btn btn-top float-right mx-2">
                    <a role="button" href="views/projects.php">
                        Projekty</a>
                </div>
                <div class="btn btn-top float-right mx-2 dropdown-toggle main" id="menuF" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <a role="button" href="views/main.php" >
                        Podstawy <i class="fas fa-chevron-circle-down"></i></a>
                </div>
                <div id="Menu-p" class="dropdown-menu text-light pepper-red w-20 p-0 mt-0 text-center" aria-labelledby="menuF">
                    <div><a role="button" href="views/main.php" class="dropdown-item px-0 active">Wstęp</a></div>
                    <div><a role="button" href="views/functions.php" class="dropdown-item px-0">Funkcje</a></div>
                    <div><a role="button" href="views/work.php" class="dropdown-item px-0">Mechanika<br/>skryptu</a></div>
                </div>

                <button type="button" class="btn text-light js-logo float-left mx-2">
                    <img title="PodstawyJS.pl" src="img/javascript-logo.png" height="28px" id="js-logo"/></a>
                </button>

            </div>

        </div>

    </div>
</nav>
<nav class="nav-s d-none">
    <div class="d-flex flex-nowrap">
        <div class="collapse" id="collNav">
            <div class="order-2 p-3 pepper-dark flex-column nav-sec " style="height: 100vh;">
                <div class="pt-5">

                    <div class="p-2 w-100 m-1 s-btn-top">
                        <a role="button" href="views/contact.php" class="">
                            <span class="">Kontakt</span></a> 
                    </div>
                    <div class="p-2 m-1 s-btn-top"><a role="button" href="views/lore.php" class="">
                            <span class="">Wiedza</span></a>
                    </div>
                    <div class="p-2 m-1 s-btn-top"><a role="button" href="views/projects.php" class="">
                            <span class="">Projekty</span></a>
                    </div>
                    <div class="p-2 m-1 s-btn-top">
                        <div class="menu">
                            <a role="button" href="views/main.php" class="">
                                <span class="">Podstawy <i class="fas fa-chevron-circle-down"></i></span></a>
                        </div>

                        <a href="views/main.php" class=" p-2 m-1 item">Wstęp</a>
                        <a href="views/functions.php" class=" p-2 m-1 item">Funkcje</a>
                        <a href="views/work.php" class=" p-2 m-1 item">Mechanika skryptu</a>

                    </div>
                </div>
            </div>
        </div>
        <div class="order-1 p-2 text-light" style="z-index: 101;">
            <button style="background-color: rgba(0,0,0, 0);" class="btn" type="button" data-toggle="collapse" data-target="#collNav" aria-expanded="false" aria-controls="collNav">
                <i class="grade pd-cont far fa-compass fa-3x"></i>
            </button>
        </div>
    </div>
</nav>



