<?php session_start(); ?>
<div class="container">
    <div class="row m-5">
        <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
             <h1 class="text-center">Kontakt</h1>
             
             <hr/>
        </div>
        
    </div>
    <div class="row mx-auto my-5">
        <div class="col-lg-12 col-xl-12  col-md-12 col-sm-12 text-justify">
            <p>
            <h5 class="text-center">Autora strony można znaleźć pod gmail'em: <span class="a-pr-info">nanquery@gmail.com</span></h5>
            
            Strona została stworzona na potrzeby wprowadzenia i zachęcenia osób nie znających programowania w ogóle.
            Jeśli autor strony pomógł Ci zrozumieć programowanie. Podoba Ci się wygląd strony. Znalazłeś błąd lub chciałbyś wyrazić opinie. Skontaktuj się wysyłając wiadomość z danymi do kontaktu lub jako anonim. Podany email nie musi być prawdziwy.
            
            </p>
        </div>
    </div>
    <div class="row mx-auto mb-3">
         <div class="col-lg-12 col-xl-12  col-md-12 col-sm-12 field">
                        <div class="field-title col-lg-3 col-xl-3 col-md-3 col-sm-3 text-center">
                            <i class="fab fa-optin-monster fa-2x li-purple"></i>
                        </div>
                        <div class="col-lg-12 col-xl-12  col-md-12 col-sm-12 p-3">
                            <form action="controllers/send.php" method="POST" action="controllers/send.php">
                        <div class="form-group">
                            <label for="name">Podaj e-mail:</label>
                            <input type="email" class="form-control" id="name" name="name" placeholder="Twój email">
                            <small id="nameHelp" class="form-text text-muted"></small>
                        </div>
                        <div class="form-group">
                            <textarea rows="5" type="text" class="form-control" id="text" name="text" placeholder="wiadomość"></textarea>
                            <small id="nameHelp" class="form-text text-muted"></small>
                            <button type="submit" class="btn btn-outline-dark btn-md text-light my-2 px-5 pepper-green">Wyślij</button>
                        </div>

                    </form>
                        </div>
                    </div>
        
            
    </div> 
    
</div>
