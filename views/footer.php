<div class='row p-5' style='background-color: #20232a;'>
    <div class='col-lg-4 col-xl-4  col-md-4 col-sm-12 border-right border-light p-5'>
        <h5><i class="fab fa-node-js"></i> Tutoriale:</h5>
        <hr class=""/>
        <div class="">
            <a href="http://kursjs.pl/index.php"  target="_blank">
                Kurs JS
            </a>
        </div>
        <div class="">
            <a href="https://www.w3schools.com/js/default.asp" target="_blank">
                W3Schools
            </a>
        </div>
        <div class="">
            <a href="https://developer.mozilla.org/pl/docs/Learn/JavaScript/Pierwsze_kroki" target="_blank">
                MDN JavaScript
            </a>
        </div>
    </div>
   <div class='col-lg-4 col-xl-4  col-md-4 col-sm-12 border-right border-light p-5'>
        <h5><i class="fas fa-terminal"></i> Pomoce:</h5>
        <hr class=""/>
        <div class="">
            <a href="https://pecetos.pl/laczy-nas-front-end/"  target="_blank">
                Łączy Nas Frontend
            </a>
        </div>
        <div class="">
            <a href="https://pl.khanacademy.org/computing/computer-programming/html-css-js/using-js-libraries-in-your-webpage/a/whats-a-js-library"  target="_blank">
               Biblioteki JavaScript
            </a>
        </div>
    </div>
    <div class='col-lg-4 col-xl-4  col-md-4 col-sm-12 p-5 l-one-f'>
        <h5><i class="fas fa-swatchbook"></i> Biblioteki:</h5>
        <hr/>
        <div class="">
            <a href="https://jquery.com/"  target="_blank">
                Jquery
            </a>
        </div>
        <div class="">
            <a href="https://reactjs.org/"  target="_blank">
                React.JS
            </a>
        </div>
    </div>
</div>
<div class="row p-2" style="background-color: #282c34;">
    <div class="col-lg-12 col-xl-12  col-md-12 col-sm-12 text-center">
        Strona internetowa <i><a href="https://podstawyjs.cba.pl">podstawyjs.cba.pl</a></i> &copy; Wszelkie prawa zastrzeżone!<br/>
    </div>
</div>