<article class="" >
    <div class="container">
        <div class="row my-5">
            <div class="col-lg-12 col-xl-12  col-md-12 col-sm-12">
                <h1 class="text-center">Funkcje</h1>
                <hr/>
                <p>
                    <span class="pl-4">Funkcje</span> są pokazaniem komputerowi od podstaw, jak wykonać dane zadanie np.<br/><br/>
                    Mamy swoją upragnioną grę, którą chcemy stworzyć. Widzi nam się wielki smok, który będzie latał z prędkością 1 macha, pluł ogniem i zabijał każdego przeciwnika na hita. I co ? <br/>
                    <span class="pl-4">Możemy to sobie narysować. Nie </span>możemy komunikować się z naszym komputerem telepatycznie. Nie zrozumie też naszego rysunku.<br/>
                    Więc czy zostaje nam gorzko zapłakać i się podać ? O tóż nie. Wciąż jest sposób, aby się dogadać z maszyną.<br/>
                    Zastanawiamy się, więc co dzieje się w naszej grze. Zaczynając od najprostszych rzeczy musimy wytłumaczyć, jak mamy poruszać naszym smokiem. Co musimy kliknąć, żeby wypusicić ogień zmieniający w popiół naszych przeciwników. Co się dzieje z życiem przeciwnika, kiedy zostanie trafiony. Itd. itp.<br/>
                    Każde z tych działań przemyślane przez nas jest <i>funkcją</i>. Sposobem na to, żeby coś się działo.<br/>
                </p>
                <p>
                    <span class="pl-4">Teraz musimy odpowiednio to zapisać.</span> Umieszczamy funkcje w kodzie JavaScript. Nadając jej nazwę. Robimy to, w taki o to sposób:<br/>

                </p>
            </div>
        </div>
        <div class="row code-theme code-bg mb-5">
            <div class="col-lg-11 col-xl-11  col-md-11 col-sm-11">

                <span class="js-code">function</span> zachowanie() <span class="js-code">{</span><br/><br/><br/>
                <span class="js-code">}</span>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-lg-12 col-xl-12  col-md-12 col-sm-12">
                <p>Zawsze używamy sformułowania <span class="js-code">function</span>. Potem podajemy nazwę naszej funkcji. Nazwa nie może zawierać znaków specjalnych, ani polskich liter. Do nazwy obowiązkowo dodajemy <i>()</i>.<br/>
                    Wewnątrz klamer <i> { ... } </i> tłumaczymy dane zachowanie, czyli co ma się dziać.

                </p>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-lg-12 col-xl-12  col-md-12 col-sm-12 p-5" style="cursor: pointer;" data-toggle="collapse" href="#start" role="button" aria-expanded="false" aria-controls="start">
                <h2>Funkcje wbudowane:</h2>
            
            </div>
        </div>
        <div class="my-5">
            <div class="row mb-0 pb-0 text-light text-center ">
                <div class="col-lg-2 col-xl-2  col-md-3 col-sm-2 p-1 wd-ch" data-toggle="collapse" href="#writeF" role="button" aria-expanded="false" aria-controls="writeF">
                    document.write();
                </div>
                <div class="col-lg-2 col-xl-2  col-md-3 col-sm-2 p-1 wd-ch wd-ch-s" data-toggle="collapse" href="#if" role="button" aria-expanded="false" aria-controls="if">
                    if();
                </div>
                <div class="col-lg-2 col-xl-2  col-md-3 col-sm-2 p-1 wd-ch wd-ch-t" data-toggle="collapse" href="#for" role="button" aria-expanded="false" aria-controls="for">
                    for();
                </div>
                <div class="col-lg-2 col-xl-2  col-md-3 col-sm-2 p-1 wd-ch wd-ch-f" data-toggle="collapse" href="#operator" role="button" aria-expanded="false" aria-controls="operator">
                    Operatory
                </div>

            </div>
            <div class="row mt-0 pt-0 border border-seconday border-rounded">
                <div class="col-lg-12 col-xl-12  col-md-12 col-sm-12 p-5">
                    <div class="collapse show" id="start">
                        <p>
                            W Js pewnych rzeczy nie musimy tłumaczyć od zera komputerowi, żeby uzyskać efekt. Dzięki temu nie wynajdujemy koła od nowa. Używamy do takich zadań funkcji wbudowanych i pomagamy sobie instrukcjami warunkowymi. Poszczególne z nich znajdziesz wyżej. </p>
                    </div>
                    <div class="collapse" id="writeF">
                        <p>
                        <h5 class="d-inline-block">document.write("tekst")</h5> - funkcja służąca dopisaniu tekstu na stronę. To co umieścisz w środku zostanie pokazane. Może to być kod html, tekst w cudzysłowach (jak wyżej) lub nawet zmienna.
                        </p>
                    </div>
                    <div class="collapse" id="if">
                        <p>
                        <h5 class="d-inline-block">if()</h5> - Dosłownie tłumacząc - jeśli/jeżeli - instrukcja warunkowa. Wewnątrz nawiasów zapisujemy warunek. Korzystamy kiedy chcemy wykonać inne działanie, tylko kiedy warunek się spełni (Prawdą jest, że). Do sprawdzenia warunku, używamy <i>przyrównania</i> (czyli: <span class="pr-cont">==</span>). W ten sposób zapisujemy:<br/>
                            warunek1=="wartość do spełnienia".<br/>
                            Wewątrz klamer - <span class="js-code">{</span> piszemy jaka magia ma się wykonać <span class="js-code">}</span>.
                            Razem z if() występuje także else - (inaczej) oraz else if - (w jeszcze innym przypadku). Robimy więc tak:
                        <div class="row code-theme code-bg m-5">
                            <div class="col-lg-11 col-xl-11  col-md-11 col-sm-11">
                                <span class="a-cont">var</span> a = 10;<br/>
                                <span class="a-cont">var</span> b = 2;<br/>
                                <span class="a-cont">var</span> c = a/b;<br/>
                                <span class="a-cont">var</span> d = 5;<br/>
                                <span class="pr-cont">if</span>(c==d) <span class="js-code">{</span><br/>
                                <div class="ml-5">document.write(<span class="lvly-content">"spełniono warunek"</span>);</div>
                                <span class="js-code">}</span><span class="pr-cont">else</span> <span class="js-code">{</span><br/>
                                <div class="ml-5">document.write(<span class="lvly-content">"Przykro mi. Warunek się nie spełnił"</span>);</div>
                                <span class="js-code">}</span> <span class="pr-cont">else if</span>(c!=d) <span class="js-code">{</span><br/><br/>
                                <div class="ml-5">document.write(<span class="lvly-content">"Tylko ten warunek się spełnił"</span>);</div>
                                <span class="js-code">}</span>
                            </div>
                        </div>
                    </div>
                    <div class="collapse" id="for">
                        <p>
                            <h5 class="d-inline-block"><i>for()</i></h5> - To <i>pętla</i>. Dzielnie nam służy, kiedy chcemy powtórzyć konkretne działania kilka razy, nie zapisując ich raz po raz.<br/> Zaczynamy w ten sposób:
                        <ul type="circle">
                            <li>Tworzymy dla naszej pętli zmienną for(<i><span class="a-cont">var</span> i=1;</i>) </li>
                            <li>Tłumaczymy jak długo nasza pętla ma się powtarzać. np. wykonuj dopóki (i jest mniejsze lub równe)  i<=3 - for(<span class="a-cont">var</span> i=1; <i>i<=3;</i>)</li>
                            <li>Informujemy, że po każdym wykonaniu pętli zwiększamy i o 1 (i++ to inaczej i=i+1) - for(<span class="a-cont">var</span> i=1; i<=3; <i>i++;</i>) </li>
                        </ul>
                        W klamrach zapisujemy, tak jak wcześniej z if'em, co ma się wydarzyć. Tym razem - co będzie się powtarzać dopóki zmienna i, nie będzie mniejsza lub równa 3. My w przykładzie poważymy się jedynie na dopisanie w naszej stronie, tego co znajduje się w zmiennej i przy każdym powtórzeniu.</p>
                        <div class="row code-theme code-bg m-5">
                            <div class="col-lg-11 col-xl-11  col-md-11 col-sm-11 py-3">
                                <span class="pr-cont">for</span>(<span class="a-cont">var</span> i=1; i<=3; i++) {
                                <div class="ml-5">document.write(i);</div>
                                }
                            </div>
                        </div>

                    </div>
                    <div class="collapse" id="operator">
                        <h4>Operatory</h4>
                        <p>
                            Używamy ich do określenie warunku lub w działaniach. Operatory od działań poznałeś na poprzedniej stronie.</p>
                        <h5 class="d-inline-block"><span class="lvly-cont">Operatory Porównania</span></h5> (używane przy if'ach) to:<br/>
                        <div class="w-75 p-2">
                            <span class="lvly-cont">==</span> <i>równe</i>
                            <hr/>
                            <span class="pr-cont">!=</span> <i>różne</i><hr/>
                            <span class="a-cont">===</span> <i>równe i taki sam typ danych - czyli spr. czy jest to np. wyraz w " ", czy też zwykła wartość</i><hr/>
                            <span class="text-danger">!==</span> <i>różne wartości i różny typ danych</i><hr/>
                            <span class="text-warning">></span> <i>większe</i><hr/>
                            <span class="js-code"><</span> <i>mniejsze</i><hr/>
                            <span class="text-danger">>=</span> <i>większe lub równe</i><hr/>
                            <span class="js-code"><=</span> <i>mniejsze lub równe</i><hr/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row m-5">

                    <div class="col-lg-12 col-xl-12  col-md-12 col-sm-12 my-5 field">
                        <div class="field-title col-lg-3 col-xl-3  col-md-3 col-sm-3 text-center">
                            <h5><i class="far fa-question-circle"></i></h5>
                        </div>
                        <div class="col-lg-12 col-xl-12  col-md-12 col-sm-12 p-3">
                            <p>
                                Tym razem nie pokazywałem zadań. Chciałbym abyś uzyskał zarys tego jak działa skrypt. Wtedy w Twojej głowie powstanie pełen obraz i już nigdy nie będziesz mieć wątpliwości, skąd - co się dzieje w JS.<br/>
                                Tak więc przejdź proszę do strony <a id="work-href" href="views/work.php">mechanika skryptu</a>. Tam też przedstawię pierwsze przykłady.<br/>
                            </p>
                        </div>
                    </div>
                </div>

    </div>
</article>
