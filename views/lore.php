<div class="container text-center">
    <div class="row mt-5">
        <div class="col-lg-12 col-xl-12  col-md-12 col-sm-12 px-5">
            <h1 class="">Wiedza</h1>
            <hr/>
        </div>

    </div>
    <div class="row mb-5 py-4">
        <div class="col-lg-12 col-xl-12  col-md-12 col-sm-12">
            <h5>Na wstępie</h5>
            <p>
                Informacje tutaj zawarte, to linki do przydatnych stron, dokumentacji i innych ciekawych miejsc. Tyle na wstępie. Jeśli masz ochotę posłuchać artykułu nad podejściem merytorycznym do programowania, zapraszam:</p>

        </div>
    </div>
    <article class=" border p-5 text-justify">
        <div class="row">
            <div class="col-lg-4 col-xl-4 col-md-4 col-sm-4 text-center mx-auto icon-box-7x">      
                        <i class="far fa-newspaper fa-7x"></i>
            </div>
            <div class="col-lg-8 col-xl-8 col-md-8 col-sm-8 mx-auto">
                <div class="col-lg-12 col-xl-12  col-md-12 col-sm-12 mx-auto ">
                    <p>
                        <span class="pl-4">Tak</span> więc może zawędrowałeś tutaj chcąc jedynie przejrzeć wszystkie podstrony ? A może jesteś spragniony wiedzy z JavaScript.<br/>
                        <span class="pl-4">Rozczaruje Cię</span>, jeśli oczekiwałeś grubo ciosanych tomów wypełnionych wiedzą z programowania. Teoria przedstawiana w książkach przez programistów nie jest zła. Na pewno znajdziesz w nich wiele wiedzy. Jednak jeśli zwrócisz uwagę [...]
                    </p>
                </div>
            </div>

        </div>
        <hr/>
        <div class="row">
            <div class="col-lg-12 col-xl-12  col-md-12 col-sm-12 my-4">
                <p>
                    <span class="pl-4">Czas</span> jaki trzeba poświęcić na zapisanie kilkuset stron wiedzy, zajmuje <u>do 3 lat</u>. Później zredagowanie treści i wprowadzanie zmian, to kolejne pół, do roku czasu. Na koniec wydanie, książkę daje się do druku. Potem ląduje na półkach, a w końcu w twoich rękach.<br/>
                <span class="pl-4">Decydując się na książkę wydaną np. w 2015 roku. Otrzymasz najnowsze informacje o języku programowania, z okresu po odjęciu jeszcze czasu, przez jaki autor pisał książkę. 
                    </p>

                    <p>
                        <span class="pl-4">Języki programowania</span>, szczególnie JavaScript zmieniają się błyskawicznie. Biblioteki, funkcje wbudowane, wykorzystywany kod. To wszystko - rozwija się. Stare części są wymieniane na nowe, lepsze. O ile nie interesuje Cię historia samego języka programowania. O tyle nie będziesz korzystać z funkcji, które lada moment mogą przestać współpracować i powodują nadmiar kodu.<br/>
                        <span class="pl-4">Musisz</span> myśleć i ułatwiać sobie prace, jeśli tylko uzyskujesz ten sam efekt.
                    </p>
            </div>
        </div>
    </article>
    <div class="row m-5">
        <div class="col-lg-12 col-xl-12  col-md-12 col-sm-12 py-4">
            <h3 class="">Strony</h3>
            <hr/>
        </div>
    </div>
    <div class="row my-5 pages mx-auto">
        <div class="card mb-5 mx-auto">
            <img class="card-img-top" src="img/w3s.png" alt="w3s">
            <div class="card-footer">
                <h5 class="card-title">W3Schools</h5>
                <p class="card-text">
                    Na tej stronie znajdziesz pełną dokumentacje JavaScript. Rozpisaną każdą funkcje, właściwość oraz wprowadzenie. Również inne języki webowe. W3S posiada zawsze aktualne informacje.
                </p>
                <small class="text-muted"><a href="https://www.w3schools.com/js/default.asp" target="_blank">Przejdź do strony</a></small>
            </div>

        </div>
        <div class="card mb-5 mx-auto">
            <img class="card-img-top" src="img/mdn.png" alt="MDN">
            <div class="card-footer">
                <h5 class="card-title">MDN Web Docs</h5>
                <p class="card-text">
                    Strona ściśle i dokładnie przedstawia wszystkie zagadnienia związane z programowaniem webowym. Jeśli zrobiłeś gdzieś błąd, tutaj nie znajdziesz prostej odpowiedzi. Jednak fundamentalne zasady działania JS.
                </p>
                <small class="text-muted"><a href="https://developer.mozilla.org/pl/docs/Learn/JavaScript/Pierwsze_kroki" target="_blank">Przejdź do strony</a></small>
            </div>
        </div>
    </div>
    <div class="row my-5 pages mx-auto">
        <div class="card mb-3 mx-auto">
            <img class="card-img-top p-5" src="img/kursjs.png" alt="kursjs">
            <div class="card-footer">
                <h5 class="card-title">Kurs JS</h5>
                <p class="card-text">
                    Strona na której jesteś, to pierwsze kroki w JS. Również później merytoryczne przykłady pokazane na projektach.
                    Pod tym linkiem znajdziesz kurs pokazujący JS od A do Z. Skorzystaj. Na prawdę warto.
                </p>
                <small class="text-muted"><a href="http://kursjs.pl/index.php" target="_blank">Przejdź do strony</a></small>
            </div>
        </div>
        <div class="card mb-3 mx-auto">
            <img class="card-img-top p-5" src="img/frontend.png" alt="pecetos">
            <div class="card-footer">
                <h5 class="card-title">Łączy Nas Frontend</h5>
                <p class="card-text">
                    Strona przedstawiająca odnośniki do kursów JavaScript i nie tylko. Wszystkie tricki, nowości, przydatne narzędzia do pisania we Frontendz'ie są tam za jednym kliknięciem. Serwis pod domeną pecetos.pl cały czas się rozwija.
                </p>
                <small class="text-muted"><a href="https://pecetos.pl/laczy-nas-front-end/" target="_blank">Przejdź do strony</a></small>
            </div>
        </div>
    </div>
    <div class="row m-5">
        <div class="col-lg-12 col-xl-12  col-md-12 col-sm-12 py-4">
            <h2 class="">Biblioteki JS</h2>
            <hr class='w-25'/>
        </div>
    </div>
    <article class="text-justify border p-5">
        <div class="row">

            <div class="col-lg-12 col-xl-12  col-md-12 col-sm-12 mx-auto text-center p-1 icon-box-9x">
                <i class="far fa-newspaper fa-9x mx-auto"></i>
            </div>

        </div>
        <div class='row'>

            <div class="col-lg-12 col-xl-12  col-md-12 col-sm-12 mx-auto">
                <div class="col-lg-12 col-xl-12  col-md-12 col-sm-12 mx-auto">
                    <p>
                        <span class="pl-4">Co to są Biblioteki?</span>
                        Kiedy będziesz robił większe i [...] większe projekty webowe zauważysz, że coraz to więcej czasu musisz im poświęcać. Wciąż opisujesz te same skrypty, które tylko odrobinę się zmieniają. Czytając to może już zakładasz -  ale przecież mogę zaprogramować funkcje, które będą łatwo rozszerzalne (do wykorzystania w każdym kodzie).</p>
                    <p> <span class="pl-4">Nie</span> mylisz się, jednak programowanie nie istnieje od dziś. Wielu ludzi już na to wpadło. Udostępniają oni w świeci swoje funkcje do konkretnych zadań, razem z ich opisem (dokumentacją), jak je używać. Slajder, ładne animacje, przejścia, pokazy slajdów, to wszystko możesz pisać sam. Jednak do tego wszystkiego istnieją zbiory funkcji, które pozwolą napisać Ci skrypt o wiele krócej. Bez wynajdywania koła od nowa. To właśnie są biblioteki.
                    </p>
                </div>
            </div>
        </div>
    </article>
    <div class="row m-5">
        <div class="col-lg-12 col-xl-12  col-md-12 col-sm-12 py-4">
            <h3 class="">Strony</h3>
            <hr/>
        </div>
    </div>
    <div class="row my-5 pages mx-auto">
        <div class="card mb-5 mx-auto">
            <img class="card-img-top" src="img/li-info.png" alt="khanacademy">
            <div class="card-footer">
                <h5 class="card-title">Wprowadzenie</h5>
                <p class="card-text">
                    Jeśli chciałbyś dowiedzieć się więcej na temat bibliotek JS, zapraszam do tego artykułu. Zostały one tam opisane z godnym pietyzmem. Polecam także przeszukać stronę. Artykuły w tej tematyce można wertować.
                </p>
                <small class="text-muted text-bottom"><a href="https://pl.khanacademy.org/computing/computer-programming/html-css-js/using-js-libraries-in-your-webpage/a/whats-a-js-library" target="_blank">Przejdź do strony</a></small>
            </div>

        </div>

    </div>
    <div class="row my-5 pages mx-auto">
        <div class="card mb-5 mx-auto">
            <img class="card-img-top p-5" src="img/jquery.png" alt="kursjs">
            <div class="card-footer">
                <h5 class="card-title">JQuery</h5>
                <p class="card-text">
                    Strona znanej biblioteki Jquery. Kiedy zdecydujesz się na swoją pierwszą bibliotekę najlepiej zacznij tutaj.
                    JQuery pozwala w prostszy i o wiele krótszy sposób zapisać kod js. Dzięki tej bibliotece jest on bardziej rozszerzalny, szybszy oraz kompatybilny z każdą przeglądarką.
                </p>
                <small class="text-muted"><a href="https://jquery.com/" target="_blank">Przejdź do strony</a></small>
            </div>
        </div>
        <div class="card mb-5 pages mx-auto">
            <img class="card-img-top p-5" style="height: 392.53px;" src="img/react.png" alt="reactjs">
            <div class="card-footer">
                <h5 class="card-title">Łączy Nas Frontend</h5>
                <p class="card-text">
                    Strona biblioteki react.js. Biblioteka pozwala tworzyć interfejsy graficzne użytkownika aplikacji internetowych. Twórcy zamieścili w niej mnóstwo funkcji, wykonujących zdarzenia działające w aplikacjach interaktywnych. Dokumentacje można znaleźć na stronie biblioteki. 
                </p>
                <small class="text-muted"><a href="https://reactjs.org/" target="_blank">Przejdź do strony</a></small>
            </div>
        </div>
    </div>
    <div class="row my-5 pages mx-auto">
        
    </div>
</div>
