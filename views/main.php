
<div class="container-fluid">

    <figure class="row my-3 p-3">
        <div class="col-lg-12 col-xl-12  col-md-12 col-sm-12 text-center">
            <div class="image"></div>
        </div>
    </figure>
</div>
<div class="container">
    <section id="start">
        <div class="row my-5 ">
            <div class="text-center col-lg-12 col-xl-12  col-sm-12 col-md-12 my-5">
                <h1 class="">Witaj!</h1>
                Znajdujesz się na stronie wprowadzającej do programowania w Javascript.
                <hr/>
                <div class="text-justify">
                    <p class="pt-2"><span class="pl-4">Postaram się</span> jak najprościej wytłumaczyć Ci tutaj, na czym polega pisanie w tym języku. Bez korzystania ze złożonych fraz i definicji w nim utartych. Nie pisząc na start o rzeczach, które wydają się zawiłe dla laika.<br/><span class="pl-4">Przekażę</span> treść merytoryczną, byś zrozumiał, jak działa skrypt. Później przedstawię mechanikę, a na koniec linki do przykładów.<br/>W innych kartach znajdziesz także kilka prostych projektów i odsyłacze do stron z wiedzą, której jeszcze nie zawarłem.<br/><span class="pl-4">Pamiętaj</span> - nie bój się samemu programować. Przede wszystkim liczy się zrozumienie i doświadczenie, jakie zdobywasz świadomie zapisując każdą linię kodu.</p>
                    <p>
                        Zanim przejdziesz dalej postaraj się ściągnąć dowolny edytor kodu. Może to być np. <span class="text-info">visual studio code</span> lub <span class="text-success">notepad++</span>.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <div class="row">
        <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
            <h2 class="text-center">Teoria</h2>
            <hr/>
            <p><h4 class="d-inline">Podstawy</h4> - rozpocznijmy od tego. Jak już wcześniej napisałem <h6 class="d-inline">Javascript</h6> jest to język programowania. Jednak czego się złapać, żeby zacząć pisać ?</p>
            <p>Zdajemy sobie sprawę, że przeglądarka nie wie, <b>jak</b> ma rozumieć jakikolwiek kod, jeżeli nie została poinformowana o języku, w którym jest on pisany. Dlatego musimy teraz to zrobić, wstawiając skrypt do naszej strony. Możliwe jest to na dwa sposoby - bezpośrednio: w kodzie HTML. Bądź zewnętrznie w osobnym pliku <i>nazwa_skryptu.js</i> Dla ułatwienia zaczniemy od pierwszego sposobu:<br/>
            <h5 class=""> Skrypt wewnętrzny </h5>
            <ul type="circle">
                <li>
                    Tworzymy nowy plik.html i z podstawowych znaczników naszą stronę.
                </li>
                <li>
                    Wewnątrz pliku html zapisujemy:
                </li>
            </ul>
            </p>
        </div>
    </div>

</div>
<div class="container">

    <div class="row code-theme code-bg mx-auto my-5">

        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mx-auto ">
            <div class="  mx-auto px-0 text-left">
                <div class="p-2 code">
                    <div class="doc-html"><?= htmlentities('<!DOCTYPE HTML>'); ?></div>
                    <div class="def-html"><?= htmlentities('<html lang="pl">'); ?></div>
                    <div class="c-child-f ml-5 def-html">
                        <?= htmlentities('<head>'); ?>
                        <div class="c-child-s ml-5">
                            <div>&lt;meta charset=<span class="in-tag">"</span><span class="even-here">utf-8</span><span class="in-tag">"</span>&gt;</div>

                        </div>
                        <div><?= htmlentities('</head>'); ?></div>
                        <div><?= htmlentities('<body>'); ?></div>
                        <div class="c-child-s ml-5 script">
                            <div class="js-code">&lt;script type=<span class="in-tag">"</span><span class="even-here">text/javascript</span><span class="in-tag">"</span>&gt;</div>
                            <div class="in-sc ml-5">
                                <span class="in-tag">
                                    // komentarz<br/>

                                </span>
                            </div>
                            <div class="js-code">&lt;/script&gt;</div>



                        </div>
                        <div><?= htmlentities('</body>'); ?></div>
                    </div>
                    <div class="def-html"><?= htmlentities('</html>'); ?></div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-xl-12  col-md-12 col-sm-12">
            <p>Teraz sposób drugi, czyli:<div class="d-inline-block"><h5 class=""> Skrpyt zewnętrzny </h5></div>. 
            <ul>
                <li>
                    Utwórzmy plik <i>skrypt.js</i>
                </li>
                <li>
                    Utwórzmy plik <i>index.html</i> w tym samym folderze, co skrypt.
                </li>
                <li>
                    Zapisujemy w pliku <i>index.html</i> naszą stronę.
                </li>
                <li>
                    Do pliku <i>index.html</i> dodajemy informacje, że korzystamy z naszego skryptu <i>skrypt.js</i> (<i class="far fa-arrow-alt-circle-down"></i>).
                </li>
            </ul>
            Piszemy ścieżkę do skryptu wewnątrz tagu - &lt;script <span class="even-here">src="skrypt.js"</span> type="text/javascript"&gt;&lt;/script&gt;
            </p>
        </div>
    </div>
</div>
<div class="container">

    <div class="row code-theme code-bg mx-auto my-5">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mx-auto">
            <div class="  mx-auto px-0 text-left">
                <div class="p-2 code">
                    <div class="doc-html"><?= htmlentities('<!DOCTYPE HTML>'); ?></div>
                    <div class="def-html"><?= htmlentities('<html lang="pl">'); ?></div>
                    <div class="c-child-f ml-5 def-html">
                        <?= htmlentities('<head>'); ?>
                        <div class="c-child-s ml-5">
                            <div>&lt;meta charset=<span class="in-tag">"</span><span class="even-here">utf-8</span><span class="in-tag">"</span>&gt;</div>

                        </div>
                        <div><?= htmlentities('</head>'); ?></div>
                        <div><?= htmlentities('<body>'); ?></div>
                        <div class="c-child-s ml-5 script">
                            <div class="js-code">&lt;script src=<span class="in-tag">"</span><span class="even-here">skrypt.js</span><span class="in-tag">"</span> type=<span class="in-tag">"</span><span class="even-here">text/javascript</span><span class="in-tag">"</span>&gt;<div class="js-code d-inline-block">&lt;/script&gt;</div>
                            </div>

                        </div>
                        <div><?= htmlentities('</body>'); ?></div>
                    </div>
                    <div class="def-html"><?= htmlentities('</html>'); ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-xl-12  col-md-12 col-sm-12 mt-5">

            <h3>Zmienne</h3>
            <p>

                <span class="pl-4">W</span> każdym języku programowania możesz je znaleźć. To swego rodzaju skrzynki, w których trzymamy marchewki. Wyobraź sobie, że idziesz do warzywniaka kupujesz kilka z nich. Wracasz i pakujesz je do skrzynki "marchewka". Od teraz już nigdy nie musisz ruszać się po marchewki. Za każdym razem, gdy bierzesz skrzynkę "marchewka" wyjmujesz marchewki identyczne i w tej samej ilości, jak te kupione ostatnio w warzywniaku i zapakowane.<br/>
                <span class="pl-4">W</span> Javascript rozróżniamy trzy rodzaje takich skrzynek:
            <ul>
                <li class="pt-3"><h4 class="lvly-cont">let</h4> - Tak samo jak ze skrzynką typu <span class="a-cont">var</span>. Jednak tę skrzynkę trzymasz w domu. Tylko ty masz dostęp. </li>
                <li class="pt-3"><h4 class="even-here">const</h4> - Pakujesz marchewki i nie dokładasz. Mrozisz je, żebyś mógł wyciągnąć na nadciągające mrozy</li>
                <li class="t-3"><h4 class="a-cont">var</h4> - Pakujesz tutaj marchewki. Za każdym razem kiedy do nich dołożysz, zmienisz ilość marchewek, które wyjmujesz ze skrzynki. Skrzynkę trzymasz na dworze, więc sąsiad może podkradać Ci marchewki. <br/><span class="pl-4">Pamiętaj</span>, że pakując cokolwiek innego (nawet pod wzgl. ilości), niż to co jest, w skrzynce podmieniasz jej zawartość.</br>
                    <span class="pl-4">Zostaniemy</span> na razie przy tej zmiennej. Można jej używać, jako zastępstwo za wszystkie trzy.</li>
            </ul>

            </p>
            <h5 class="pb-0 mb-0">Praktyczny przykład</h5> <span class="pl-4">W naszym wewnętrznym skrypcie stworzymy</span> dwie zmienne z przypisanymi wartościami. Potem je pomnożymy. Trzecia stworzona zmienna będzie pustą skrzynką, do której włożymy wynik mnożenia.<br/>
            Na końcu używając <i>alert(liczbaT)</i> wyrzucimy na ekran wartość trzeciej zmiennej.
        </div>
    </div>
</div>
<div class="container">

    <div class="row code-theme code-bg mx-auto my-5">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mx-auto">
            <div class="   mx-auto px-0 text-left">
                <div class=" code">
                    <span class="a-cont">var</span> liczbaP = 5;<br/>
                    <span class="a-cont">var</span> liczbaD = 2;<br/>
                    <span class="a-cont">var</span> liczbaT;<br/>
                    liczbaT = LiczbaP*liczbaD;<br/><br/>
                    alert(liczbaT);
                </div>
            </div>
        </div>
    </div>
    <div class="row m-3">

        <div class="col-lg-12 col-xl-12  col-md-12 col-sm-12 field">
            <div class="field-title col-lg-3 col-xl-3  col-md-3 col-sm-3 text-center">
                <h5>Zadanie</h5>
            </div>
            <div class="col-lg-12 col-xl-12  col-md-12 col-sm-12 p-3">
                <p>Wykonaj działania ze zmiennymi.<br/> Dla trzech podanych zmiennych podstaw zamiast "<span class="text-warning">*</span>" - kolejno "<span class="text-warning">+</span>", "<span class="text-warning">-</span>", "<span class="text-warning">/</span>", "<span class="text-warning">%</span>".<br/>
                    Spróbuj po działaniu użyć jeszcze zapisu liczbaT<i>++</i> oraz liczbaT<i>--</i>.<br/> Sprawdzaj jakie wyniki otrzymasz.</p>
            </div>
        </div>
    </div>
</div>