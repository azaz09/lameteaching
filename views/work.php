<div class="container-fluid fn">
    <div class="row mt-5 mx-5 ">
        <div class="col-lg-12 col-xl-12  col-md-12 col-sm-12">
            <h1 class="text-center">Mechanika Skryptu</h1>
            <hr/>
            <p>Zinterpretujmy zatem całe działanie skryptu. Ujmując to przez jego mechanikę. Inaczej są to składniki, które stosujemy do otrzymania działającego skryptu:
            </p>
        </div>

    </div>
    <div class="card-columns text-center middle">
        <div class="card mx-auto my-2" data-toggle="collapse" data-target="#uchwyt" aria-expanded="true" aria-controls="uchwyt">
            <h4 class="card-header a-lg">Uchwyt</h4>
            <div class="card-body bg-dark">
                <div class="card-text">np. getElementById();</div>
            </div>
        </div>
        <div class="collapse" id="uchwyt">
            <div class="card p-3 bg-dark" >
                <p>Żeby odnieść się do elementu na stronie, np. zapisanego div'a o danym id, którego treść chcemy zmienić, potrzebujemy się go złapać. Robimy to przyczepiając się do niego - po id, nazwie tag'u lub też class'ie.
                </p>
            </div>
        </div>
        <div class="card mx-auto my-2" data-toggle="collapse" data-target="#idk" aria-expanded="true" aria-controls="idk">
            <h4 class="card-header a-or">Identyfikatory</h4>
            <div class="card-body bg-dark">

                <div class="card-text">np. id="nazwa_elementu";</div>
            </div>
        </div>
        <div class="collapse" id="idk">
            <div class="card p-3 bg-dark" >
                <p>To o czym była mowa wcześniej (w uchwytach). Teraz od strony html. Żeby odnieść się do stworzenego tagu/elementu, w HTML potrzebujemy nadać mu id lub class'ę, podobnie, jak to używaliśmy do css.<br/> Jak pewnie zauważyłeś po class'ie możemy odnieść się nawet do kilku elementów.
                </p>
            </div>
        </div>
        <div class="card mx-auto my-2" data-toggle="collapse" data-target="#origin" aria-expanded="true" aria-controls="origin">
            <h4 class="card-header a-gr">Wprowadzanie</h4>
            <div class="card-body bg-dark">
                <div class="card-text">np. document.write();</div>
            </div>
        </div>
        <div class="collapse" id="origin">
            <div class="card p-3 bg-dark" >
                <p>Żeby wprowadzać wartość - tekst, zmienne, które będą wyświetlane na stronie używamy funkcji wbudowanej write().
                    Teraz możesz zobaczyć, że document - to tak na prawdę to, za co chwytamy. Używając tutaj "write()" wprowadzamy, do tego za co złapaliśmy.
                </p>
            </div>
        </div>
        <div class="card mx-auto my-2" data-toggle="collapse" data-target="#pr" aria-expanded="true" aria-controls="pr">
            <h4 class="card-header a-fruit">Właściwości</h4>
            <div class="card-body bg-dark">
                <div class="card-text">np. value, innerHTML;</div>
            </div>
        </div>
        <div class="collapse" id="pr">
            <div class="card p-3 bg-dark" >
                <p>Co Jednak kiedy chcemy pobrać wartość ? Mamy formularz z polami do wprowadzania danych. Na kliknięcie pobieramy ich wartość. Więc musimy złapać - to - pojemnik (tu. input). Wiadomo, że jeśli każdy input nazwiemy tak samo, to różne wartości rozbiegną się, niczym króliki na polowaniu. Dlatego każdy musi mieć unikalną nazwę. Potem po uchwyceniu wyjmujemy ich wartość po kropce dodając "value".<br/> Wartość możemy też od razu podmienić w niektórych przypadkach - jak .innerHTML = "nowa zawartość uchwyconego elementu".
                </p>
            </div>
        </div>
        <div class="card mx-auto my-2" data-toggle="collapse" data-target="#built" aria-expanded="true" aria-controls="built">
            <h4 class="card-header lvly-blue">Funkcje wbudowane</h4>
            <div class="card-body bg-dark">
                <div class="card-text">np. setTimeout();</div>
            </div>
        </div>
        <div class="collapse" id="built">
            <div class="card p-3 bg-dark" >
                <p>Tak jak write(). Istnieją pewne funkcje, które wykonują określone zadania. Zamiast wynajdywać koło od nowa możemy ich użyć. Nic na tym nie stracimy. setTimeout("nazwa_funkcji()", <span class="lvly-cont">5000</span>) jest funkcją, która po określonym <span class="lvly-cont">czasie</span> wykona dane zadanie (inaczej wywoła funkcje, która jest tym zadaniem), czas podaje się w milisekundach.  
                </p>
            </div>
        </div>
        <div class="card mx-auto my-2" data-toggle="collapse" data-target="#events" aria-expanded="true" aria-controls="events">
            <h4 class="card-header a-green">Zdarzenia</h4>
            <div class="card-body bg-dark">
                <div class="card-text">np. onload="nazwa_funkcji();",<br/> onclick="nazwa_funkcji();";</div>
            </div>
        </div>
        <div class="collapse" id="events">
            <div class="card p-3 bg-dark" >
                <p>Dopóki nie użyjemy zdarzenia żadna magia się nie wykona. W końcu to użytkownik korzysta z programu. A nie program wysysa życiodajne siły z nas. Musimy więc powiedzieć, jakie zadanie, w jakim przypadku ma się wykonać. Onload - na załadowanie. onclick- na kliknięcie. Więc tworzymy naszą funkcje wykonaj(), która w klamrach ma zapisane prompt("informacje", "wprowadzanie"). Po tym tworzymy przycisk na naszej stronie i dodajemy wewnątrz tagu - zdarzenie <i>onclick="wykonaj();"</i> . Jak widać po kliknięciu wywołujemy naszą funkcje, której zadaniem jest pokazać okno dialogowe.
                </p>
            </div>
        </div>
        
            <div class="card mx-auto my-2" data-toggle="collapse" data-target="#methods" aria-expanded="true" aria-controls="methods">
                <h4 class="card-header a-red">Własne funkcje</h4>
                <div class="card-body bg-dark ">
                    <div class="card-text">function moja_funkcja() {...}</div>
                </div>
            </div>
            <div class="collapse" id="methods">
                <div class="card p-3 bg-dark" >
                    <p>Własne funkcje. To zadania, które chcemy wykonać. Tłumaczymy w klamrach maszynie co ma się dziać. Piszemy tam nasz skrypt. A potem wywołując funkcje zdarzeniem, ożywa ta część skryptu, która jest w tym wnętrzu.
                    </p>
                </div>
            </div>
    </div>
</div>